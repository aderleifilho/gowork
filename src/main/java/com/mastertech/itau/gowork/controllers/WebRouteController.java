package com.mastertech.itau.gowork.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebRouteController {

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@RequestMapping("/mapa")
	public String mapa() {
		return "mapa";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/cadastro")
	public String cadastro() {
		return "cadastro";
	}
}
