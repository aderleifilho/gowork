const botaoLogin = document.querySelector("#botaoLogin");
botaoLogin.onclick = fazerLogin;

function fazerLogin(){
    var usuario = document.querySelector("#usuario").value;
    var senha = document.querySelector("#senha").value;
    var obj = {"username":usuario, "password":senha};
    if (!usuario && !senha){
    	alert("USUARIO/SENHA EM BRANCO!");
    	return false;
    }
    else
    	if (!usuario){
    		alert("USUARIO EM BRANCO!");
    		return false;
    	}
    	else 
    		if (!senha){
    			alert("SENHA EM BRANCO!");
    			return false;
    	}
    
    post('/login', obj).then(function(dados) {});
} 

function post(url, data){
	
	return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json'
        }
    })
    .then(function(response) { 
    	if (response.status == 200) {
    		window.location = "/mapa";
    	}
        return response.json();
    })
    .catch(function(err) { 
        alert("Usuário/Senha incorreto!")
        return err;
    });
}

function get(url, data){
    return fetch(url, {
        method: 'GET'
    })
    .then(function(response) { 
        return response.json();
    })
    .catch(function(err) { 
        console.error(err) 
    });
}
